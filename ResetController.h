#pragma once
#include "Controller.h"

class ResetController : public Controller {
public:
  virtual void action(const bbox_t &bbox) override;
};
