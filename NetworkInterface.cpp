#include "NetworkInterface.h"

NetworkInterface::NetworkInterface(const char *addr, const char *port) {
  WSADATA wsadata;
  ADDRINFOA hints;
  PADDRINFOA result;

  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if ((WSAStartup(MAKEWORD(2, 2), &wsadata)) != 0) {
    perror("WsaStartup FAILED");
    exit(EXIT_FAILURE);
  }

  if ((getaddrinfo(addr, port, &hints, &result)) != 0) {
    perror("GetAddrInfo FAILED");
    exit(EXIT_FAILURE);
  }

  if ((clientsocket = socket(result->ai_family, result->ai_socktype,
                             result->ai_protocol)) == INVALID_SOCKET) {
    perror("Socket FAILED");
    exit(EXIT_FAILURE);
  }

  if ((connect(clientsocket, result->ai_addr, result->ai_addrlen)) ==
      SOCKET_ERROR) {
    perror("Connect FAILED");
    exit(EXIT_FAILURE);
  }

  freeaddrinfo(result);
}

void NetworkInterface::sendCommand(const char *command) {
  send(clientsocket, command, strlen(command), 0);
}
