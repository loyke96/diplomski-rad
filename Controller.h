#pragma once
#include "NetworkInterface.h"
#include <yolo_v2_class.hpp>

class Controller {
protected:
  static int oldx;
  static NetworkInterface netint;

public:
  virtual void action(const bbox_t &bbox) = 0;
  static void noAction();
};
