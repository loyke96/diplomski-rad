#include "ResetController.h"

constexpr auto DEFAULT_RESET = "\3";

void ResetController::action(const bbox_t &bbox) {
  netint.sendCommand(DEFAULT_RESET);
  oldx = -1;
}
