#pragma once
#include "Controller.h"

class MoveController : public Controller {
public:
  virtual void action(const bbox_t &bbox) override;
};
