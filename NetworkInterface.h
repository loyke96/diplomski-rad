#pragma once

#include <WS2tcpip.h>
#include <WinSock2.h>

constexpr auto DEFAULT_ADDR = "10.21.11.222";
constexpr auto DEFAULT_PORT = "9999";

class NetworkInterface {
private:
  SOCKET clientsocket = INVALID_SOCKET;

public:
  NetworkInterface(const char *addr = DEFAULT_ADDR,
                   const char *port = DEFAULT_PORT);
  void sendCommand(const char *command);
};
