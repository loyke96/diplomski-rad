#include "MoveController.h"

constexpr auto DEFAULT_LEFT = "\4";
constexpr auto DEFAULT_RIGHT = "\5";

void MoveController::action(const bbox_t &bbox) {
  int newX = (bbox.x + bbox.w) / 2;
  if (oldx != -1) {
    if (oldx < newX)
      netint.sendCommand(DEFAULT_LEFT);
    else
      netint.sendCommand(DEFAULT_RIGHT);
  }
  oldx = newX;
}
