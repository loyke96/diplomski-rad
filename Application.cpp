#include "Application.h"

#include "MoveController.h"
#include "ResetController.h"

#include <iostream>
#include <opencv2/core/mat.hpp>
#include <opencv2/videoio.hpp>
#include <vector>
#include <yolo_v2_class.hpp>

using namespace cv;
using namespace std;

constexpr auto DEFAULT_CONFIDENCE = 0.8;

void Application::run() {
  Detector detector = Detector("yolo.cfg", "yolo.weights");
  ResetController reset = ResetController();
  MoveController move = MoveController();
  VideoCapture videoCapture = VideoCapture(0);
  Mat frame = Mat();

  while (videoCapture.isOpened()) {
    videoCapture >> frame;
    vector<bbox_t> bboxes = detector.detect(frame, DEFAULT_CONFIDENCE);

    if (bboxes.size() > 0) {
      bbox_t bbox = bboxes[0];
      if (bbox.obj_id == 0) {
        move.action(bbox);
      } else {
        reset.action(bbox);
      }
    } else {
      Controller::noAction();
    }
  }
}
